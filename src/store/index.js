import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const gitlabOrigin = 'https://gitlab.com/api/v4/projects/?search=';
export default new Vuex.Store({
  state: {
    loaded: false,
    rawData: null,
    userMessage: '',
  },
  mutations: {
    updateMessage(state, message) {
      state.userMessage = message;
    },
    fillUpData(state, response) {
      state.rawData = response;
      state.loaded = true;
    },
  },
  actions: {
    fetchData({ commit, state }) {
      return axios
        .get(`${gitlabOrigin}${state.userMessage}`)
        .then(response => {
          commit('fillUpData', response.data);
        })
        .catch(e => alert(`Не удалось получить данные с сервера по причине ${e}. Попробуйте обновить страницу.`));
    },
  },
  getters: {
    getProjectPerId: state => id => {
      return state.rawData.find(obj => obj.id === id);
    },
  },
});
