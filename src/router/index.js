import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../components/Home.vue';
import Modal from '../components/Modal';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [
      {
        path: ':id',
        name: 'projectModal',
        component: Modal,
        meta: {
          showModal: true,
        },
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
