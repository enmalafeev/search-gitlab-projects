import { Given, Then } from 'cypress-cucumber-preprocessor/steps';

const url = 'https://search-gitlab-projects.now.sh/';

Given('i open the main page', () => {
  cy.visit(url);
  cy.get('form').contains('form', 'Search');
});

Then(/^user type a "(\S+)" to search element$/, number => {
  cy.get('input').type(number);
  cy.get('button').click();
});
Then('user see table of projects was rendered on page', () => {
  cy.get('table').should('not.empty');
});
