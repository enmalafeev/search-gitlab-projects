import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import SearchForm from '@/components/SearchForm.vue';

const localVue = createLocalVue();

localVue.use(Vuex);

describe('Actions.vue', () => {
  let state;
  let mutations;
  let actions;
  let store;

  beforeEach(() => {
    state = {
      userMessage: '',
    };

    mutations = {
      updateMessage: jest.fn(),
    };

    store = new Vuex.Store({
      state,
      mutations,
    });
  });

  it('searchForm component contains form', () => {
    const wrapper = shallowMount(SearchForm, { store, localVue });
    expect(wrapper.contains('form')).toBe(true);
  });

  it('searchForm update input field', () => {
    const wrapper = shallowMount(SearchForm, { store, localVue });
    const input = wrapper.find('input');
    input.element.value = 'random';
    input.trigger('input');
    expect(mutations.updateMessage).toHaveBeenCalled();
  });
});
